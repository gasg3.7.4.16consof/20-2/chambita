"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const database_1 = __importDefault(require("../database"));
class EmpleadorController {
    list(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const empleador = yield database_1.default.query('SELECT * FROM empleadores');
            res.json(empleador);
        });
    }
    getOne(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const { idEmpleador } = req.params;
            const empleador = yield database_1.default.query('SELECT * FROM empleadores WHERE idEmpleador = ?', [idEmpleador]);
            console.log(empleador.length);
            if (empleador.length > 0) {
                return res.json(empleador[0]);
            }
            res.status(404).json({ text: "El Empleador no existe" });
        });
    }
    create(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const result = yield database_1.default.query('INSERT INTO empleadores set ?', [req.body]);
            res.json({ message: 'Guardar Usuario' });
        });
    }
    update(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const { idEmpleador } = req.params;
            const oldEmpleador = req.body;
            yield database_1.default.query('UPDATE empleadores set ? WHERE idEmpleador = ?', [req.body, idEmpleador]);
            res.json({ message: "El Empleador a sido actualizado" });
        });
    }
    delete(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const { idEmpleador } = req.params;
            yield database_1.default.query('DELETE FROM empleadores WHERE idEmpleador = ?', [idEmpleador]);
            res.json({ message: "El Empleador ha sido eliminado" });
        });
    }
}
const empleadorController = new EmpleadorController;
exports.default = empleadorController;
