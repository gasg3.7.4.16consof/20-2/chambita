"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
module.exports = {
    isLoggedIn(req, res, next) {
        if (req.isAuthenticated()) {
            return next();
        }
        return res.redirect('/signin');
    }
};
