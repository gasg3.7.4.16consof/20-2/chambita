import { Router } from 'express';
import EmpleadorController from '../controllers/empleadorController';


class EmpleadorRoutes {

    public router: Router = Router();

    constructor() {
        this.config();
    }

    config(): void {
        this.router.get('/', EmpleadorController.list);
        this.router.get('/:idEmpleador', EmpleadorController.getOne);
        this.router.post('/', EmpleadorController.create);
        this.router.put('/:idEmpleador', EmpleadorController.update);
        this.router.delete('/:idEmpleador', EmpleadorController.delete);
    }

}

const empleadorRoutes = new EmpleadorRoutes();
export default empleadorRoutes.router;