import { Request, Response } from 'express';
import jwt from 'jsonwebtoken';
import pool from '../database';

class LoginController {

    public async login(req: Request, res: Response): Promise<void> {
        const { correo, clave } = req.params;
        const usuario = await pool.query('SELECT id, correo FROM usuario WHERE correo=? AND clave=?', [correo, clave]);
        
        res.send(usuario[0] || { id: null, correo: null });
    }
}

const loginController = new LoginController;
export default loginController;