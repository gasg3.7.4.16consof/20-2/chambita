import { Request, Response } from 'express';

class IndexController {

    public index(req: Request, res: Response) {
        res.send('Backend Chambitas');
    }
}

export const indexController = new IndexController;