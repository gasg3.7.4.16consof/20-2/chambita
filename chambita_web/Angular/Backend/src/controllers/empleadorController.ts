import { Request, Response } from 'express';
import pool from '../database';

class EmpleadorController {

    public async list(req: Request, res: Response): Promise<void> {
        const empleador = await pool.query('SELECT * FROM empleadores');
        res.json(empleador);
    }

    public async getOne(req: Request, res: Response): Promise<any> {
        const { idEmpleador } = req.params;
        const empleador = await pool.query('SELECT * FROM empleadores WHERE idEmpleador = ?', [idEmpleador]);
        console.log(empleador.length);
        if (empleador.length > 0) {
            return res.json(empleador[0]);
        }
        res.status(404).json({ text: "El Empleador no existe" });
    }

    public async create(req: Request, res: Response): Promise<void> {
        const result = await pool.query('INSERT INTO empleadores set ?', [req.body]);
        res.json({ message: 'Guardar Usuario' });
    }

    public async update(req: Request, res: Response): Promise<void> {
        const { idEmpleador } = req.params;
        const oldEmpleador = req.body;
        await pool.query('UPDATE empleadores set ? WHERE idEmpleador = ?', [req.body, idEmpleador]);
        res.json({ message: "El Empleador a sido actualizado" });
    }

    public async delete(req: Request, res: Response): Promise<void> {
        const { idEmpleador } = req.params;
        await pool.query('DELETE FROM empleadores WHERE idEmpleador = ?', [idEmpleador]);
        res.json({ message: "El Empleador ha sido eliminado" });
    }
}

const empleadorController = new EmpleadorController;
export default empleadorController;