import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { Routes, RouterModule } from '@angular/router';
import { ConfirmationPopoverModule } from 'angular-confirmation-popover';
import { ReactiveFormsModule} from '@angular/forms';
import { FormsModule } from '@angular/forms';

/*Angular Material*/
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';

import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterUserComponent } from './components/register-user/register-user.component';
import { HomeComponent } from './components/home/home.component';
import { LTrabajosComponent } from './components/ltrabajos/ltrabajos.component';
import { TformularioComponent } from './components/tformulario/tformulario.component';
import { TdetallesComponent } from './components/tdetalles/tdetalles.component';
import { EdetallesComponent } from './components/edetalles/edetalles.component';
import { EmpleadoresComponent } from './components/empleadores/empleadores.component';
//import { EformularioComponent } from './components/eformulario/eformulario.component';
import { UsuariosService } from './services/usuarios.service';
import { EmpleadoresService } from './services/empleadores.service';
import { TrabajosService } from './services/trabajos.service';
import { TrabajoDetallesService } from './services/trabajoDetalles.service';
import { FiltroTrabajosPipe } from './pipes/filtro-trabajos.pipe';
import { FiltroEmpleadoresPipe } from './pipes/filtro-empleadores.pipe';

//login con redes sociales
import { SocialLoginModule, SocialAuthServiceConfig } from 'angularx-social-login';
import { GoogleLoginProvider, FacebookLoginProvider } from 'angularx-social-login';

const appRoutes: Routes = [
  { path: 'chambitas', component: AppComponent},
  { path: 'login', component: LoginComponent},
  { path: 'registeruser', component: RegisterUserComponent},
  { path: 'tformulario', component: TformularioComponent},
  { path: 'home', component: HomeComponent},
  { path: 'ltrabajos', component: LTrabajosComponent},
  { path: 'detalles', component: TdetallesComponent},
  { path: 'detalles/:idTrabajo', component: TdetallesComponent},
  { path: 'lempleadores', component: EmpleadoresComponent},
  //{ path: 'eformulario', component: EformularioComponent},
  { path: 'edetalles', component: EdetallesComponent},
  { path: 'edetalles/:idEmpleador', component: EdetallesComponent}

];

@NgModule({
  declarations: [AppComponent, NavbarComponent, LoginComponent, RegisterUserComponent, HomeComponent, LTrabajosComponent, TformularioComponent, TdetallesComponent, EdetallesComponent, EmpleadoresComponent, FiltroTrabajosPipe, FiltroEmpleadoresPipe],
  
  imports: [RouterModule.forRoot(appRoutes), BrowserModule, FormsModule, HttpClientModule, SocialLoginModule, ReactiveFormsModule, MatInputModule, MatFormFieldModule, ConfirmationPopoverModule.forRoot({
      confirmButtonType: 'danger',}),],
  providers: [
    UsuariosService,
    {
      provide: 'SocialAuthServiceConfig',
      useValue: {
        autoLogin: true,
        providers: [
          {
            id: GoogleLoginProvider.PROVIDER_ID,
            provider: new GoogleLoginProvider(
              '624796833023-clhjgupm0pu6vgga7k5i5bsfp6qp6egh.apps.googleusercontent.com'
            ),
          },
          {
            id: FacebookLoginProvider.PROVIDER_ID,
            provider: new FacebookLoginProvider('697725514264462'),
          }
        ],
      } as SocialAuthServiceConfig,
    }
  ],
  bootstrap: [AppComponent],
})
export class AppModule { }