import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Empleador } from '../models/Empleadores';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})

export class EmpleadoresService {

  API_URI = 'http://localhost:3000/api';

  constructor(private http: HttpClient) { }

  getEmpleadores() {
    return this.http.get(`${this.API_URI}/empleadores`);
  }

  getEmpleador(idEmpleador: string) {
    return this.http.get(`${this.API_URI}/empleadores/${idEmpleador}`);
  }

  deleteEmpleador(idEmpleador: string) {
    return this.http.delete(`${this.API_URI}/empleadores/${idEmpleador}`);
  }

  saveEmpleador(empleador: Empleador) {
    return this.http.post(`${this.API_URI}/empleadores`, empleador);
  }

  updateEmpleador(idEmpleador: string|number, updateEmpleador: Empleador): Observable<Empleador> {
    return this.http.put(`${this.API_URI}/empleadores/${idEmpleador}`, updateEmpleador);
  }
}