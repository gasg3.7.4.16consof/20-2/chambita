import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})

export class LoginService {

  API_URI = 'http://localhost:3000/api';

  constructor(private http: HttpClient, private router: Router) { }

  loginUser(user) {
	return this.http.post(`${this.API_URI}/login`, user);
  }

  logout() {
	this.router.navigate(['/home']);
  }
}