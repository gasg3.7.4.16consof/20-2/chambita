import { Component, OnInit, HostBinding, Output, EventEmitter } from '@angular/core';
import { Empleador } from 'src/app/models/Empleadores';
import { EmpleadoresService } from '../../services/Empleadores.service';
import { Router } from '@angular/router';
import { FormControl } from '@angular/forms';
import { debounceTime } from 'rxjs/operators';
import { ChangeDetectionStrategy } from '@angular/core';
import {BehaviorSubject} from 'rxjs';

//import Swal from 'sweetalert2';

@Component({
  selector: 'app-empleadores',
  templateUrl: './empleadores.component.html',
  styleUrls: ['./empleadores.component.css']
})
export class EmpleadoresComponent implements OnInit {

  @HostBinding('class') classes = 'center';

  filterPoster='';
  empleadores: any = [];

  constructor(private router: Router, private EmpleadoresService: EmpleadoresService) {}

  ngOnInit() {
    this.getEmpleadores();
  }

  getEmpleadores() {
    this.EmpleadoresService.getEmpleadores()
      .subscribe(
        res => {
          this.empleadores = res;
        },
        err => console.error(err)
      );
  }

  getEmpleador(Empleadores: any) {
      let link=['/edetalles', Empleadores.idEmpleador];
      this.router.navigate(link);
  }

  deleteEmpleadores(idEmpleador: string) {
    this.EmpleadoresService.deleteEmpleador(idEmpleador)
      .subscribe(
        res => {
          console.log(res);
          this.getEmpleadores();
        },
        err => console.error(err)
      )
  }
}