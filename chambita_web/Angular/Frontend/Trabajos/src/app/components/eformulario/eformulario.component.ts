import { Component, OnInit, HostBinding } from '@angular/core';
import { Empleador } from 'src/app/models/Empleadores';
import { EmpleadoresService } from 'src/app/services/empleadores.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-eformulario',
  templateUrl: './eformulario.component.html',
  styleUrls: ['./eformulario.component.css']
})
export class EformularioComponent implements OnInit {

@HostBinding('class') clases = 'center';

  empleadores: Empleador = {
    idEmpleador: 0,
    dniEmpleador: 0,
    nombre: '',
    apellido: '',
    clave: '',
    numTelefono: 0,
    correo: '',
    ciudad: '',
    distrito: '',
    foto: '',
    cuentaValidada: 0
  };

  edit: boolean = false;

  constructor(private empleadoresService: EmpleadoresService, private router: Router, private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    const params = this.activatedRoute.snapshot.params;
    if (params.idEmpleador) {
      this.empleadoresService.getEmpleador(params.idEmpleador)
        .subscribe(
          res => {
            console.log(res);
            this.empleadores = res;
            this.edit = true;
          },
          err => console.log(err)
        )
    }
  }

  saveNewEmpleador() {
    delete this.empleadores.idEmpleador;
    this.empleadoresService.saveEmpleador(this.empleadores)
      .subscribe(
        res => {
          console.log(res);
          this.router.navigate(['/home']);
        },
        err => console.error(err)
      )
  }

  updateEmpleador() {
    this.empleadoresService.updateEmpleador(this.empleadores.idEmpleador, this.empleadores)
      .subscribe(
        res => { 
          console.log(res);
          this.router.navigate(['/home']);
        },
        err => console.error(err)
      )
  }
}