import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EformularioComponent } from './eformulario.component';

describe('TformularioComponent', () => {
  let component: EformularioComponent;
  let fixture: ComponentFixture<EformularioComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EformularioComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EformularioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
