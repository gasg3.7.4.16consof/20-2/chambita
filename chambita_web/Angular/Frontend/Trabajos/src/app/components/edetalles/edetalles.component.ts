import { Component, OnInit, HostBinding } from '@angular/core';
import { EmpleadoresService } from '../../services/Empleadores.service';
import { Empleador } from 'src/app/models/Empleadores';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-edetalles',
  templateUrl: './edetalles.component.html',
  styleUrls: ['./edetalles.component.css']
})
export class EdetallesComponent implements OnInit {
  @HostBinding('class') classes = 'center';

  form: FormGroup;
  empleador: any = [];

  constructor(private route: ActivatedRoute, private router: Router, private empleadoresService: EmpleadoresService) { }

  ngOnInit() {
    this.getTrabajos();
    let idEmpleador= this.route.snapshot.params['idEmpleador'];
    if(!idEmpleador) return;

    this.empleadoresService.getEmpleador(idEmpleador)
    .subscribe(
      rs => this.empleador = rs,
      err => console.log(err),
      () => {
        if(this.empleador.length > 0){
          this.form.patchValue({
            idEmpleador: this.empleador[0].idEmpleador,
            dniEmpleador: this.empleador[0].dniEmpleador,
            nombre: this.empleador[0].nombre,
            apellido: this.empleador[0].apellido,
            clave: this.empleador[0].clave,
            numTelefono: this.empleador[0].numTelefono,
            correo: this.empleador[0].correo,
            ciudad: this.empleador[0].ciudad,
            distrito: this.empleador[0].distrito,
            foto: this.empleador[0].foto,
            cuentaValidada: this.empleador[0].cuentaValidada
          })
        }
      }
      )
  }

  getTrabajos() {
    this.empleadoresService.getEmpleadores()
      .subscribe(
        res => {
          this.empleador = res;
        },
        err => console.error(err)
      );
  }

  
}