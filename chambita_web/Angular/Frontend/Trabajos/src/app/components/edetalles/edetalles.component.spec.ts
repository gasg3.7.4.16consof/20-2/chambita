import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EdetallesComponent } from './edetalles.component';

describe('TdetallesComponent', () => {
  let component: EdetallesComponent;
  let fixture: ComponentFixture<EdetallesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EdetallesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EdetallesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
