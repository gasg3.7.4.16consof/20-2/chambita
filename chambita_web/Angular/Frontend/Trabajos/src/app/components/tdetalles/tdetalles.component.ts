import { Component, OnInit, HostBinding } from '@angular/core';
import { TrabajoDetallesService } from '../../services/trabajoDetalles.service';
import { TrabajoDetalles } from 'src/app/models/TrabajoDetalles';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-tdetalles',
  templateUrl: './tdetalles.component.html',
  styleUrls: ['./tdetalles.component.css']
})
export class TdetallesComponent implements OnInit {
  @HostBinding('class') classes = 'center';

  form: FormGroup;
  trabajo: any = [];

  constructor(private route: ActivatedRoute, private router: Router, private trabajoDetallesService: TrabajoDetallesService) { }

  ngOnInit() {
    this.getTrabajos();
    let idTrabajo= this.route.snapshot.params['idTrabajo'];
    if(!idTrabajo) return;

    this.trabajoDetallesService.getTrabajo(idTrabajo)
    .subscribe(
      rs => this.trabajo = rs,
      err => console.log(err),
      () => {
        if(this.trabajo.length > 0){
          this.form.patchValue({
            idTrabajo: this.trabajo[0].idTrabajo,
            nombreTrabajo: this.trabajo[0].nombreTrabajo,
            descTrabajo: this.trabajo[0].descTrabajo,
            pagoTrabajo: this.trabajo[0].pagoTrabajo,
            plazoPostulacion: this.trabajo[0].plazoPostulacion,
            distrito: this.trabajo[0].distrito,
            direccion: this.trabajo[0].direccion,
            nombre: this.trabajo[0].nombre,
            apellidos: this.trabajo[0].apellidos,
            numTelefono: this.trabajo[0].numTelefono,
            correo: this.trabajo[0].correo
          })
        }
      }
      )
  }

  getTrabajos() {
    this.trabajoDetallesService.getTrabajos()
      .subscribe(
        res => {
          this.trabajo = res;
        },
        err => console.error(err)
      );
  }

  
}