import { Component, OnInit, HostBinding, Output, EventEmitter } from '@angular/core';
import { TrabajosService } from '../../services/trabajos.service';
import { Trabajo } from 'src/app/models/Trabajos';
import { TrabajoDetalles } from 'src/app/models/TrabajoDetalles';
import { TrabajoDetallesService } from '../../services/trabajoDetalles.service';
import { Router } from '@angular/router';
import { FormControl } from '@angular/forms';
import { debounceTime } from 'rxjs/operators';
import { ChangeDetectionStrategy } from '@angular/core';
import {BehaviorSubject} from 'rxjs';

//import Swal from 'sweetalert2';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  @HostBinding('class') classes = 'center';

  filterPost='';
  trabajos: any = [];

  constructor(private router: Router, private trabajoService: TrabajosService) {}

  ngOnInit() {
    this.getTrabajos();
  }

  getTrabajos() {
    this.trabajoService.getTrabajos()
      .subscribe(
        res => {
          this.trabajos = res;
        },
        err => console.error(err)
      );
  }

  getTrabajo(Trabajo: any) {
      let link=['/detalles', Trabajo.idTrabajo];
      this.router.navigate(link);
  }

  deleteTrabajos(idTrabajo: string) {
    this.trabajoService.deleteTrabajo(idTrabajo)
      .subscribe(
        res => {
          console.log(res);
          this.getTrabajos();
        },
        err => console.error(err)
      )
  }
}