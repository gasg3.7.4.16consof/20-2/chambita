import { Title } from '@angular/platform-browser';

export interface SocialUser {
	provider?: string,
    id?: number,
    email?: string,
    photoUrl?: string,
    nro_celular?: string
};
