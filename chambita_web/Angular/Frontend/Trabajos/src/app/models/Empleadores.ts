import { Title } from '@angular/platform-browser';

export interface Empleador {
    idEmpleador?: number,
    dniEmpleador?: number,
    nombre?: string,
    apellido?: string,
    clave?: string,
    numTelefono?: number,
    correo?: string,
    ciudad?: string,
    distrito?: string,
    foto?: string,
    cuentaValidada?: number
};