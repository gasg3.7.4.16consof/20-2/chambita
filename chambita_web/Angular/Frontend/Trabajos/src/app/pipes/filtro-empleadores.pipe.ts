import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filtroEmpleadores'
})
export class FiltroEmpleadoresPipe implements PipeTransform {

  transform(value: any,  arg:string): any {
	const resultPost=[];
	for(const empleadores  of value){
		if(empleadores.nombre.toUpperCase().includes(arg.toUpperCase()) || empleadores.ciudad.toUpperCase().includes(arg.toUpperCase())){
			resultPost.push(empleadores);
		};
  	};
  	return resultPost;
  }
}
