import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filtroTrabajos'
})
export class FiltroTrabajosPipe implements PipeTransform {

  transform(value: any,  arg:string): any {
	const resultPost=[];
	for(const trabajos  of value){
		if(trabajos.nombreTrabajo.toUpperCase().includes(arg.toUpperCase()) || trabajos.distrito.toUpperCase().includes(arg.toUpperCase())){
			resultPost.push(trabajos);
		};
  	};
  	return resultPost;
  }
}
