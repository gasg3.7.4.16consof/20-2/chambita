-- phpMyAdmin SQL Dump
-- version 5.0.3
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 24-11-2020 a las 16:13:33
-- Versión del servidor: 10.4.14-MariaDB
-- Versión de PHP: 7.2.34

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `chambitas`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `empleadorcalificaciones`
--

CREATE TABLE `empleadorcalificaciones` (
  `idCalificacion` int(11) NOT NULL,
  `idEmpleador` int(11) NOT NULL,
  `dniTrabajador` int(11) NOT NULL,
  `numCalificacion` int(11) NOT NULL DEFAULT 0,
  `Comentario` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `empleadores`
--

CREATE TABLE `empleadores` (
  `idEmpleador` int(11) NOT NULL,
  `dniEmpleador` int(11) NOT NULL,
  `nombre` varchar(255) NOT NULL,
  `apellido` varchar(255) NOT NULL,
  `clave` varchar(255) NOT NULL,
  `numTelefono` int(11) NOT NULL,
  `correo` varchar(255) NOT NULL,
  `ciudad` varchar(255) NOT NULL,
  `distrito` varchar(255) NOT NULL,
  `foto` varchar(255) NOT NULL,
  `cuentaValidada` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `empleadores`
--

INSERT INTO `empleadores` (`idEmpleador`,`dniEmpleador`, `nombre`, `apellido`, `clave`, `numTelefono`, `correo`, `ciudad`, `distrito`, `foto`, `cuentaValidada`) VALUES
(1, 70580909, 'Jhoel', 'Huallpar Dorado', 'jhoel21', 984490903, 'jhoellithus@gmail.com', 'Cusco', 'Cusco', 'Photo', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `etiquetas`
--

CREATE TABLE `etiquetas` (
  `idEtiqueta` int(11) NOT NULL,
  `titulo` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `horarios`
--

CREATE TABLE `horarios` (
  `idHorario` int(11) NOT NULL,
  `idTrabajo` int(11) NOT NULL,
  `dia` varchar(255) NOT NULL,
  `horaInicio` time NOT NULL,
  `horaFinal` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `trabajadorcalificaciones`
--

CREATE TABLE `trabajadorcalificaciones` (
  `idCalificacion` int(11) NOT NULL,
  `idTrabajador` int(11) NOT NULL,
  `idEmpleador` int(11) NOT NULL,
  `numCalificacion` int(11) NOT NULL DEFAULT 0,
  `comentario` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `trabajadores`
--

CREATE TABLE `trabajadores` (
  `dni` int(11) NOT NULL,
  `nombres` varchar(255) NOT NULL,
  `apellidos` varchar(255) NOT NULL,
  `clave` varchar(255) NOT NULL,
  `fechaNacimiento` date DEFAULT NULL,
  `numTelefono` int(11) NOT NULL,
  `correo` varchar(255) NOT NULL,
  `ciudad` varchar(255) NOT NULL,
  `distrito` varchar(255) NOT NULL,
  `foto` varchar(255) NOT NULL,
  `descripcion` varchar(750) NOT NULL,
  `cuentaValidada` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `trabajadores`
--

INSERT INTO `trabajadores` (`dni`, `nombres`, `apellidos`, `clave`, `fechaNacimiento`, `numTelefono`, `correo`, `ciudad`, `distrito`, `foto`, `descripcion`, `cuentaValidada`) VALUES
(70580909, 'Jhoel', 'Huallpar Dorado', '984490903', '0000-00-00', 984490903, 'jhoellithus@gmail.com', 'Cusco', 'Cusco', 'hoalala', 'fdfddsddddfdfdfdfdfdfdf', 0),
(705809092, 'jhoel', 'add', 'dfdfdf', '2020-10-13', 5656, 'gggghhghgghhghh', 'hhghghghghghgh', 'hghghghghhghg', 'hhhghghgh', 'hhghghghghghghg', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `trabajador_etiquetas`
--

CREATE TABLE `trabajador_etiquetas` (
  `idRelacion` int(11) NOT NULL,
  `dniTrabajador` int(11) NOT NULL,
  `idEtiqueta` int(11) NOT NULL,
  `nivel` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `trabajador_trabajos`
--

CREATE TABLE `trabajador_trabajos` (
  `idRelacion` int(11) NOT NULL,
  `dniTrabajador` int(11) NOT NULL,
  `idTrabajo` int(11) NOT NULL,
  `fechaInicio` date NOT NULL,
  `fechaFinal` date NOT NULL,
  `estado` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `trabajos`
--

CREATE TABLE `trabajos` (
  `idTrabajo` int(11) NOT NULL,
  `nombreTrabajo` varchar(255) NOT NULL,
  `descTrabajo` varchar(750) NOT NULL,
  `pagoTrabajo` decimal(10,2) NOT NULL,
  `estado` varchar(25) NOT NULL DEFAULT 'Activo',
  `fechaInicio` date NOT NULL,
  `fechaFinal` date NOT NULL,
  `plazoPostulacion` date NOT NULL,
  `distrito` varchar(255) NOT NULL,
  `direccion` varchar(255) NOT NULL,
  `idEmpleador` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `trabajos`
--

INSERT INTO `trabajos` (`idTrabajo`, `nombreTrabajo`, `descTrabajo`, `pagoTrabajo`, `estado`, `fechaInicio`, `fechaFinal`, `plazoPostulacion`, `distrito`, `direccion`, `idEmpleador`) VALUES
(10, 'Mecanico', 'Mecanico con experiencia para trabajos de medio tiempo bbb hola mundo.', '250.50', '1', '2020-11-09', '2020-11-17', '2020-11-15', 'Cusco', 'Av. Independencia', 1),
(143, 'Mecanico', 'Mecanico con experiencia para trabajos de medio tiempo bbb hola mundo.', '250.50', '1', '2020-11-09', '2020-11-17', '2020-11-15', 'Cusco', 'Av. Independencia', 1),
(144, 'Mecanico', 'Mecanico con experiencia para trabajos de medio tiempo bbb hola mundo.', '250.50', '1', '2020-11-09', '2020-11-17', '2020-11-15', 'Cusco', 'Av. Independencia', 1),
(145, 'Mecanico', 'Mecanico con experiencia para trabajos de medio tiempo bbb hola mundo.', '250.50', '1', '2020-11-09', '2020-11-17', '2020-11-15', 'Cusco', 'Av. Independencia', 1),
(146, 'Mecanico', 'Mecanico con experiencia para trabajos de medio tiempo bbb hola mundo.', '250.50', '1', '2020-11-09', '2020-11-17', '2020-11-15', 'Cusco', 'Av. Independencia', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `trabajo_etiquetas`
--

CREATE TABLE `trabajo_etiquetas` (
  `idRelacion` int(11) NOT NULL,
  `idTrabajo` int(11) NOT NULL,
  `idEtiqueta` int(11) NOT NULL,
  `prioridad` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE `usuario` (
  `id` int(11) NOT NULL,
  `correo` varchar(250) DEFAULT NULL,
  `clave` varchar(25) DEFAULT NULL,
  `nro_celular` varchar(15) DEFAULT NULL,
  `tipoUsuario` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`id`, `correo`, `clave`, `nro_celular`, `tipoUsuario`) VALUES
(1, 'jhoel@gmail.com', '984490903', '984490903', ''),
(2, 'jhoel@gmail.com', 'jhoel45', '123456784', ''),
(3, 'jhoel2121@gmail.com', '123456789', '984490903', ''),
(4, 'jhoel2112@gmail.com', 'jhoel12345', '984490903', ''),
(5, 'jhoell@gmail.com', '123456789', '987654321', ''),
(6, 'jhoel2112@gmail.com', '984490903', '984490903', ''),
(7, 'jhoel2112@gmail.com', '984490903', '984490903', ''),
(8, 'jhoel2112@gmail.com', '984490903', '984490903', ''),
(9, 'jhoel@gmail.com', '984490903', '984490903', ''),
(10, 'jhoel@gmail.com', 'jhoel45qa', '984490903', ''),
(11, 'jhoel@gmail.com', '984490903', '984490903', ''),
(12, 'jhoel@gmail.com', '12345678', '984490903', ''),
(13, 'jhoel@gmail.com', '984490903', '984490903', ''),
(14, 'jhoelsd@gmail.com', 'aaaaaaaaaa', '111111111', ''),
(15, 'jhoelsd@gmail.com', 'aaaaaaaaaa', '111111111', ''),
(16, 'jhoelsd@gmail.com', 'aaaaaaaaaa', '111111111', ''),
(17, 'jhoelsd@gmail.com', 'aaaaaaaaaa', '111111111', ''),
(18, 'jhowlsd@gmail.com', 'asasasasas', '333333333', ''),
(19, 'jhowlsd@gmail.com', 'asasasasas', '333333333', ''),
(20, 'frans@gmail.com', '123456', '984490903', ''),
(21, 'frans@gmail.com', 'jhoel45', '984490903', 'Trabajador'),
(22, 'jhoelsd@gmail.com', '984490903', '984490903', 'Empleador'),
(23, 'jhoelsd@gmail.com', 'aaaaaaaaaa', '111111111', 'Empleador'),
(24, 'jhowlsd@gmail.com', 'asasasasas', '333333333', 'Empleador');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `empleadorcalificaciones`
--
ALTER TABLE `empleadorcalificaciones`
  ADD PRIMARY KEY (`idCalificacion`),
  ADD KEY `idEmpleador` (`idEmpleador`),
  ADD KEY `dniTrabajador` (`dniTrabajador`);

--
-- Indices de la tabla `empleadores`
--
ALTER TABLE `empleadores`
  ADD PRIMARY KEY (`idEmpleador`);

--
-- Indices de la tabla `etiquetas`
--
ALTER TABLE `etiquetas`
  ADD PRIMARY KEY (`idEtiqueta`);

--
-- Indices de la tabla `horarios`
--
ALTER TABLE `horarios`
  ADD PRIMARY KEY (`idHorario`),
  ADD KEY `idTrabajo` (`idTrabajo`);

--
-- Indices de la tabla `trabajadorcalificaciones`
--
ALTER TABLE `trabajadorcalificaciones`
  ADD PRIMARY KEY (`idCalificacion`),
  ADD KEY `idEmpleador` (`idEmpleador`),
  ADD KEY `idTrabajador` (`idTrabajador`);

--
-- Indices de la tabla `trabajadores`
--
ALTER TABLE `trabajadores`
  ADD PRIMARY KEY (`dni`);

--
-- Indices de la tabla `trabajador_etiquetas`
--
ALTER TABLE `trabajador_etiquetas`
  ADD PRIMARY KEY (`idRelacion`),
  ADD KEY `dniTrabajador` (`dniTrabajador`),
  ADD KEY `idEtiqueta` (`idEtiqueta`);

--
-- Indices de la tabla `trabajador_trabajos`
--
ALTER TABLE `trabajador_trabajos`
  ADD PRIMARY KEY (`idRelacion`),
  ADD KEY `dniTrabajador` (`dniTrabajador`),
  ADD KEY `idTrabajo` (`idTrabajo`);

--
-- Indices de la tabla `trabajos`
--
ALTER TABLE `trabajos`
  ADD PRIMARY KEY (`idTrabajo`),
  ADD KEY `idEmpleador` (`idEmpleador`);

--
-- Indices de la tabla `trabajo_etiquetas`
--
ALTER TABLE `trabajo_etiquetas`
  ADD PRIMARY KEY (`idRelacion`),
  ADD KEY `idEtiqueta` (`idEtiqueta`),
  ADD KEY `idTrabajo` (`idTrabajo`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `empleadorcalificaciones`
--
ALTER TABLE `empleadorcalificaciones`
  MODIFY `idCalificacion` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `etiquetas`
--
ALTER TABLE `etiquetas`
  MODIFY `idEtiqueta` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `horarios`
--
ALTER TABLE `horarios`
  MODIFY `idHorario` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `trabajadorcalificaciones`
--
ALTER TABLE `trabajadorcalificaciones`
  MODIFY `idCalificacion` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `trabajador_etiquetas`
--
ALTER TABLE `trabajador_etiquetas`
  MODIFY `idRelacion` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `trabajador_trabajos`
--
ALTER TABLE `trabajador_trabajos`
  MODIFY `idRelacion` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `trabajos`
--
ALTER TABLE `trabajos`
  MODIFY `idTrabajo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=150;

--
-- AUTO_INCREMENT de la tabla `trabajo_etiquetas`
--
ALTER TABLE `trabajo_etiquetas`
  MODIFY `idRelacion` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `usuario`
--
ALTER TABLE `usuario`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT de la tabla `empleadores`
--
ALTER TABLE `empleadores`
  MODIFY `idEmpleador` int(11) NOT NULL AUTO_INCREMENT;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `empleadorcalificaciones`
--
ALTER TABLE `empleadorcalificaciones`
  ADD CONSTRAINT `empleadorcalificaciones_ibfk_1` FOREIGN KEY (`idEmpleador`) REFERENCES `empleadores` (`idEmpleador`),
  ADD CONSTRAINT `empleadorcalificaciones_ibfk_2` FOREIGN KEY (`dniTrabajador`) REFERENCES `trabajadores` (`dni`);

--
-- Filtros para la tabla `horarios`
--
ALTER TABLE `horarios`
  ADD CONSTRAINT `horarios_ibfk_1` FOREIGN KEY (`idTrabajo`) REFERENCES `trabajos` (`idTrabajo`);

--
-- Filtros para la tabla `trabajadorcalificaciones`
--
ALTER TABLE `trabajadorcalificaciones`
  ADD CONSTRAINT `trabajadorcalificaciones_ibfk_1` FOREIGN KEY (`idEmpleador`) REFERENCES `empleadores` (`idEmpleador`),
  ADD CONSTRAINT `trabajadorcalificaciones_ibfk_2` FOREIGN KEY (`idTrabajador`) REFERENCES `trabajadores` (`dni`);

--
-- Filtros para la tabla `trabajador_etiquetas`
--
ALTER TABLE `trabajador_etiquetas`
  ADD CONSTRAINT `trabajador_etiquetas_ibfk_1` FOREIGN KEY (`dniTrabajador`) REFERENCES `trabajadores` (`dni`),
  ADD CONSTRAINT `trabajador_etiquetas_ibfk_2` FOREIGN KEY (`idEtiqueta`) REFERENCES `etiquetas` (`idEtiqueta`);

--
-- Filtros para la tabla `trabajador_trabajos`
--
ALTER TABLE `trabajador_trabajos`
  ADD CONSTRAINT `trabajador_trabajos_ibfk_1` FOREIGN KEY (`dniTrabajador`) REFERENCES `trabajadores` (`dni`),
  ADD CONSTRAINT `trabajador_trabajos_ibfk_2` FOREIGN KEY (`idTrabajo`) REFERENCES `trabajos` (`idTrabajo`);

--
-- Filtros para la tabla `trabajos`
--
ALTER TABLE `trabajos`
  ADD CONSTRAINT `trabajos_ibfk_1` FOREIGN KEY (`idEmpleador`) REFERENCES `empleadores` (`idEmpleador`);

--
-- Filtros para la tabla `trabajo_etiquetas`
--
ALTER TABLE `trabajo_etiquetas`
  ADD CONSTRAINT `trabajo_etiquetas_ibfk_1` FOREIGN KEY (`idEtiqueta`) REFERENCES `etiquetas` (`idEtiqueta`),
  ADD CONSTRAINT `trabajo_etiquetas_ibfk_2` FOREIGN KEY (`idTrabajo`) REFERENCES `trabajos` (`idTrabajo`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
