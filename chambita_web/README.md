# Chambita

## Aplicación web

El objetivo de este proyecto es realizar un aplicativo web para Chambita.
En el presente directorio encontraremos:

Código de la App Chambita web Link[https://gitlab.com/gasg3.7.4.16consof/20-2/chambita/-/tree/master/chambita_web].

Video de la instalación de las dependencias de Angular y Node.

Documentación de la App Chambita.

## Herramientas:

#### Node js
#### Angular
#### Sublime text
#### xampp


##### Código:
La carpeta código Angular en donde encontraremos el Backend, la base de datos y el frontend

##### Videos:
Video 1: Video para la ejecución de Chambita y las instalacion de las dependencias para el bakend y el frontend


### Dependencias

Node 12.14.0
Angular 10.1.4

### Comando de instalación de las Dependencias

npm install "Dependencia"

### Backend *** Node

"core-js"

"cors"

"express"

"morgan"

"promise-mysql"

"typescript"

"@types/cors"

"@types/express"

"@types/morgan"

"@types/mysql"

"nodemon"


### Frontend* Angular

"angular-confirmation-popover":

"angularx-social-login"

"core-js"

"ng-validation"

"rxjs"

"tslib"

"zone.js"

"typescript"


### Compilar Backend: 

npm run build 

npm run dev

### Compilar el Frondend

ng serve -o


#### Autor:
Jhoel Huallpar Dorado
jhuallpard@ulasalle.edu.pe
