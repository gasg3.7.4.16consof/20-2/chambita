import 'package:flutter_test/flutter_test.dart';
import 'package:chambita_movil/libs/validator_email_password.dart';

void main() {
  group('Validator', () {
    final validator = Validator();

    test('Password Test', () {
      expect(validator.validatePassword(''),
          PasswordValidationResults.EMPTY_PASSWORD);
      expect(validator.validatePassword('passw'),
          PasswordValidationResults.TOO_SHORT);
      expect(validator.validatePassword('validPass'),
          PasswordValidationResults.VALID);
    });

    test('Email Test', () {
      expect(validator.validateEmail(''), EmailValidationResults.EMPTY_EMAIL);
      expect(validator.validateEmail('email.com'),
          EmailValidationResults.NON_VALID);
      expect(validator.validateEmail('email@hmail.1'),
          EmailValidationResults.NON_VALID);
      expect(validator.validateEmail('email@gmail.com'),
          EmailValidationResults.VALID);
    });
  });
}
