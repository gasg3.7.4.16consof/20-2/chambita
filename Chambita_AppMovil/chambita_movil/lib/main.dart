import 'package:chambita_movil/helpers/dependency_inject.dart';
import 'package:chambita_movil/screens/menu_dashboard.dart';
import 'package:chambita_movil/screens/menu_screen.dart';
import 'package:chambita_movil/screens/trabajo_info.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:chambita_movil/screens/login_screen.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:firebase_auth/firebase_auth.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  DependencyInjection.initialize();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Login UI',
      debugShowCheckedModeBanner: false,
      //home: LoginScreen(),
      //home: SignInDemo(),
      home: MenuScreen(),
      //home: TrabajoInfo(),
    );
  }
}
