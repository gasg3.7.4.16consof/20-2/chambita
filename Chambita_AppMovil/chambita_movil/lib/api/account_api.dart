import 'package:chambita_movil/helpers/http.dart';
import 'package:chambita_movil/helpers/http_response.dart';
import 'package:chambita_movil/models/usuario_trabajador.dart';

class AccountApi {
  final Http _http;

  AccountApi(this._http);

  Future<HttpResponse<UserTrabajador>> getEmpleadorInfo() {
    _http.request(
      '/api/empleadores/11111111',
      method: "GET",
      data: {"dniEmpleador": 11111111},
      parser: (data) {
        return UserTrabajador.fromJson(data);
      },
    );
  }
}
