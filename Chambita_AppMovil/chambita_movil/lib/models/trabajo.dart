import 'package:meta/meta.dart' show required;

class Trabajo {
  Trabajo({
    @required this.idTrabajo,
    @required this.nombreTrabajo,
    @required this.descTrabajo,
    @required this.pagoTrabajo,
    @required this.estado,
    @required this.fechaInicio,
    @required this.fechaFinal,
    @required this.plazoPostulacion,
    @required this.distrito,
    @required this.direccion,
    @required this.dniEmpleador,
  });

  final int idTrabajo;
  final String nombreTrabajo;
  final String descTrabajo;
  final int pagoTrabajo;
  final int estado;
  final DateTime fechaInicio;
  final DateTime fechaFinal;
  final DateTime plazoPostulacion;
  final String distrito;
  final String direccion;
  final int dniEmpleador;

  factory Trabajo.fromJson(Map<String, dynamic> json) => Trabajo(
        idTrabajo: json["idTrabajo"],
        nombreTrabajo: json["nombreTrabajo"],
        descTrabajo: json["descTrabajo"],
        pagoTrabajo: json["pagoTrabajo"],
        estado: json["estado"],
        fechaInicio: DateTime.parse(json["fechaInicio"]),
        fechaFinal: DateTime.parse(json["fechaFinal"]),
        plazoPostulacion: DateTime.parse(json["plazoPostulacion"]),
        distrito: json["distrito"],
        direccion: json["direccion"],
        dniEmpleador: json["dniEmpleador"],
      );

  Map<String, dynamic> toJson() => {
        "idTrabajo": idTrabajo,
        "nombreTrabajo": nombreTrabajo,
        "descTrabajo": descTrabajo,
        "pagoTrabajo": pagoTrabajo,
        "estado": estado,
        "fechaInicio": fechaInicio.toIso8601String(),
        "fechaFinal": fechaFinal.toIso8601String(),
        "plazoPostulacion": plazoPostulacion.toIso8601String(),
        "distrito": distrito,
        "direccion": direccion,
        "dniEmpleador": dniEmpleador,
      };
}
