import 'package:meta/meta.dart' show required;

class AuthenticationResponse {
  final String dniEmpleador;
  final int nombre;

  AuthenticationResponse({
    @required this.dniEmpleador,
    @required this.nombre,
  });

  static AuthenticationResponse fromJson(Map<String, dynamic> json) {
    return AuthenticationResponse(
      dniEmpleador: json['dniEmpleador'],
      nombre: json['nombre'],
    );
  }
}
