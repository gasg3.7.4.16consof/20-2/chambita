import 'package:meta/meta.dart' show required;

class UserTrabajador {
  UserTrabajador({
    @required this.dni,
    @required this.nombres,
    @required this.apellidos,
    @required this.clave,
    @required this.fechaNacimiento,
    @required this.numTelefono,
    @required this.correo,
    @required this.ciudad,
    @required this.distrito,
    @required this.foto,
    @required this.descripcion,
    @required this.cuentaValidada,
  });

  final int dni;
  final String nombres;
  final String apellidos;
  final String clave;
  final DateTime fechaNacimiento;
  final int numTelefono;
  final String correo;
  final String ciudad;
  final String distrito;
  final String foto;
  final String descripcion;
  final int cuentaValidada;

  factory UserTrabajador.fromJson(Map<String, dynamic> json) => UserTrabajador(
        dni: json["dni"],
        nombres: json["nombres"],
        apellidos: json["apellidos"],
        clave: json["clave"],
        fechaNacimiento: DateTime.parse(json["fechaNacimiento"]),
        numTelefono: json["numTelefono"],
        correo: json["correo"],
        ciudad: json["ciudad"],
        distrito: json["distrito"],
        foto: json["foto"],
        descripcion: json["descripcion"],
        cuentaValidada: json["cuentaValidada"],
      );

  Map<String, dynamic> toJson() => {
        "dni": dni,
        "nombres": nombres,
        "apellidos": apellidos,
        "clave": clave,
        "fechaNacimiento": fechaNacimiento.toIso8601String(),
        "numTelefono": numTelefono,
        "correo": correo,
        "ciudad": ciudad,
        "distrito": distrito,
        "foto": foto,
        "descripcion": descripcion,
        "cuentaValidada": cuentaValidada,
      };
}
