import 'package:chambita_movil/api/account_api.dart';
import 'package:chambita_movil/api/autentication_api.dart';
import 'package:chambita_movil/helpers/http.dart';
import 'package:dio/dio.dart';
import 'package:get_it/get_it.dart';
import 'package:logger/logger.dart';

abstract class DependencyInjection {
  static void initialize() {
    final Dio dio = Dio(
      BaseOptions(baseUrl: 'http://10.0.2.2:3000'),
    );
    Logger logger = Logger();
    Http _http = Http(
      dio: dio,
      logger: logger,
      logsEnabled: true,
    );
    final autenticationApi _autenticationApi = autenticationApi(_http);
    GetIt.instance.registerSingleton<autenticationApi>(_autenticationApi);

    final AccountApi accountApi = AccountApi(_http);
    GetIt.instance.registerSingleton<AccountApi>(accountApi);
  }
}
