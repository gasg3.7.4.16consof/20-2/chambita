# Tabla de contenidos 

[TOCM]

[TOC]

# Flutter - ChambitaMóvil

El objetivo de este proyecto es realizar un aplicativo móvil para Chambita. 
En el presente directorio encontraremos:

* Código de la App Chambita  [Código link](https://gitlab.com/gasg3.7.4.16consof/20-2/chambita/-/tree/master/Chambita_AppMovil/chambita_movil "Código link")
* Video de introducción a Flutter [VideoIntroFlutter link](https://gitlab.com/gasg3.7.4.16consof/20-2/chambita/-/blob/master/Chambita_AppMovil/Introducción%20a%20flutter.mp4 "VideoIntroFlutter link" )
* Video para la ejecución de Chambita App  [VideoLoNecesarioChambita link](https://gitlab.com/gasg3.7.4.16consof/20-2/chambita/-/blob/master/Chambita_AppMovil/Lo_necesario_para_ejecutar_chambita_AppMovil.mp4 "VideoLoNecesarioChambita link")
* Documentación de la App Chambita

## Herramientas:

* Flutter
* Memu Emulator 
* Visual Studio Code
* Android Studio


## Código:
La carpeta código se encuentra el nombre chambita_movil

## Videos:

* Introducción a Flutter: [VideoIntroFlutter link](https://gitlab.com/gasg3.7.4.16consof/20-2/chambita/-/blob/master/Chambita_AppMovil/Introducción%20a%20flutter.mp4 "VideoIntroFlutter link" )
    * ¿Qué es Flutter?
    * Estructura de Flutter
    * Herramientas que usamos con Flutter
    * Primeros pasos con Flutter


* Video 2: Video para la ejecución de Chambita App  [VideoLoNecesarioChambita link](https://gitlab.com/gasg3.7.4.16consof/20-2/chambita/-/blob/master/Chambita_AppMovil/Lo_necesario_para_ejecutar_chambita_AppMovil.mp4 "VideoLoNecesarioChambita link")
    * ¿Qué necesitamos para ejecutar la aplicación de Chambita?
    * Repaso breve del video introductorio a Flutter





## Autor:
Jhonny Frans Gallegos Mendoza
jgallegosm@ulasalle.edu.pe

