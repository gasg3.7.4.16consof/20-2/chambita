-- phpMyAdmin SQL Dump
-- version 5.0.3
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 09-12-2020 a las 04:23:17
-- Versión del servidor: 10.4.14-MariaDB-log
-- Versión de PHP: 7.2.34

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `chambita_bd`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `empleadorcalificaciones`
--

CREATE TABLE `empleadorcalificaciones` (
  `idCalificacion` int(11) NOT NULL,
  `dniEmpleador` int(11) NOT NULL,
  `dniTrabajador` int(11) NOT NULL,
  `numCalificacion` int(11) NOT NULL DEFAULT 0,
  `Comentario` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `empleadores`
--

CREATE TABLE `empleadores` (
  `dniEmpleador` int(11) NOT NULL,
  `nombre` varchar(255) NOT NULL,
  `apellido` varchar(255) NOT NULL,
  `clave` varchar(255) NOT NULL,
  `numTelefono` int(11) NOT NULL,
  `correo` varchar(255) NOT NULL,
  `ciudad` varchar(255) NOT NULL,
  `distrito` varchar(255) NOT NULL,
  `foto` varchar(255) NOT NULL,
  `cuentaValidada` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `empleadores`
--

INSERT INTO `empleadores` (`dniEmpleador`, `nombre`, `apellido`, `clave`, `numTelefono`, `correo`, `ciudad`, `distrito`, `foto`, `cuentaValidada`) VALUES
(11111111, 'nombre1', 'apellido1', '11111111', 111111111, '11111111@gmail.com', 'ciudad1', 'distrito1', '', 0),
(12121212, 'probando', '---', 'usuario3', 0, 'usuario3@gmail.com', '---', '---', '', 0),
(66666666, 'probando', '---', '123456789', 0, 'perrograo@gmail.com', '---', '---', '', 0),
(2147483647, 'probando3', '---', 'usuario-1', 0, 'usuario-1@gmail.com', '---', '---', '', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `etiquetas`
--

CREATE TABLE `etiquetas` (
  `idEtiqueta` int(11) NOT NULL,
  `titulo` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `horarios`
--

CREATE TABLE `horarios` (
  `idHorario` int(11) NOT NULL,
  `idTrabajo` int(11) NOT NULL,
  `dia` varchar(255) NOT NULL,
  `horaInicio` time NOT NULL,
  `horaFinal` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sessions`
--

CREATE TABLE `sessions` (
  `sid` varchar(255) NOT NULL,
  `sess` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL CHECK (json_valid(`sess`)),
  `expired` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `sessions`
--

INSERT INTO `sessions` (`sid`, `sess`, `expired`) VALUES
('KXINHcSYtAvjiqSX59KE3TRfOOuLNh77', '{\"cookie\":{\"originalMaxAge\":86400000,\"expires\":\"2020-12-10T00:36:33.194Z\",\"httpOnly\":true,\"path\":\"/\"}}', '2020-12-10 01:40:27');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `trabajadorcalificaciones`
--

CREATE TABLE `trabajadorcalificaciones` (
  `idCalificacion` int(11) NOT NULL,
  `idTrabajador` int(11) NOT NULL,
  `idEmpleador` int(11) NOT NULL,
  `numCalificacion` int(11) NOT NULL DEFAULT 0,
  `comentario` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `trabajadores`
--

CREATE TABLE `trabajadores` (
  `dni` int(11) NOT NULL,
  `nombres` varchar(255) NOT NULL,
  `apellidos` varchar(255) NOT NULL,
  `clave` varchar(255) NOT NULL,
  `fechaNacimiento` date DEFAULT NULL,
  `numTelefono` int(11) NOT NULL,
  `correo` varchar(255) NOT NULL,
  `ciudad` varchar(255) NOT NULL,
  `distrito` varchar(255) NOT NULL,
  `foto` varchar(255) NOT NULL,
  `descripcion` varchar(750) NOT NULL,
  `cuentaValidada` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `trabajadores`
--

INSERT INTO `trabajadores` (`dni`, `nombres`, `apellidos`, `clave`, `fechaNacimiento`, `numTelefono`, `correo`, `ciudad`, `distrito`, `foto`, `descripcion`, `cuentaValidada`) VALUES
(88888888, 'trabajador2', 'apletrabajador2', '8888888', '2020-12-22', 888888888, '88888888@gmail.com', 'lima', 'lima', '', '', 0),
(99999999, 'trabajador1', 'apetrabajador1', '99999999', '2020-10-22', 999999999, '99999999@§gmail.com', 'arequipa', 'cayma', '', '', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `trabajador_etiquetas`
--

CREATE TABLE `trabajador_etiquetas` (
  `idRelacion` int(11) NOT NULL,
  `dniTrabajador` int(11) NOT NULL,
  `idEtiqueta` int(11) NOT NULL,
  `nivel` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `trabajador_trabajos`
--

CREATE TABLE `trabajador_trabajos` (
  `idRelacion` int(11) NOT NULL,
  `dniTrabajador` int(11) NOT NULL,
  `idTrabajo` int(11) NOT NULL,
  `fechaInicio` date NOT NULL,
  `fechaFinal` date NOT NULL,
  `estado` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `trabajos`
--

CREATE TABLE `trabajos` (
  `idTrabajo` int(11) NOT NULL,
  `nombreTrabajo` varchar(255) NOT NULL,
  `descTrabajo` varchar(750) NOT NULL,
  `pagoTrabajo` decimal(10,2) NOT NULL,
  `estado` int(11) NOT NULL DEFAULT 1,
  `fechaInicio` date NOT NULL,
  `fechaFinal` date NOT NULL,
  `plazoPostulacion` date NOT NULL,
  `distrito` varchar(255) NOT NULL,
  `direccion` varchar(255) NOT NULL,
  `dniEmpleador` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `trabajos`
--

INSERT INTO `trabajos` (`idTrabajo`, `nombreTrabajo`, `descTrabajo`, `pagoTrabajo`, `estado`, `fechaInicio`, `fechaFinal`, `plazoPostulacion`, `distrito`, `direccion`, `dniEmpleador`) VALUES
(1, 'Trabajo1', 'destrabajo1', '120.00', 1, '2020-11-27', '2020-11-28', '2020-11-25', 'cayma', 'cayma, unjo dos', 11111111),
(2, 'Apoyo para mudanza', 'Se necesita personal para que ayude en la mudanza', '15.00', 1, '2020-12-01', '2020-12-04', '2020-11-02', 'cayma', 'Av cayma cayma cayma', 11111111),
(3, 'Apoyo para mudanza', 'Se necesita personal para que ayude en la mudanza', '15.00', 1, '2020-12-01', '2020-12-04', '2020-11-02', 'cayma', 'Av cayma cayma cayma', 11111111);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `trabajo_etiquetas`
--

CREATE TABLE `trabajo_etiquetas` (
  `idRelacion` int(11) NOT NULL,
  `idTrabajo` int(11) NOT NULL,
  `idEtiqueta` int(11) NOT NULL,
  `prioridad` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` int(11) UNSIGNED NOT NULL,
  `email` varchar(45) NOT NULL,
  `password` varchar(255) NOT NULL,
  `salt` varchar(255) NOT NULL,
  `firstname` varchar(255) DEFAULT NULL,
  `lastname` varchar(255) DEFAULT NULL,
  `username` varchar(45) DEFAULT NULL,
  `roles` varchar(255) DEFAULT 'user',
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `provider` varchar(255) DEFAULT 'local',
  `provider_data` mediumtext DEFAULT NULL,
  `provider_data_plus` mediumtext DEFAULT NULL,
  `provider_ids` text DEFAULT NULL,
  `reset_password_token` varchar(45) DEFAULT NULL,
  `reset_password_expires` timestamp(6) NULL DEFAULT NULL,
  `email_verification_token` varchar(255) DEFAULT NULL,
  `email_verified` tinyint(1) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `_evolutions`
--

CREATE TABLE `_evolutions` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) NOT NULL,
  `titleDown` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `batch` int(11) DEFAULT NULL,
  `checksum` varchar(255) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `_evolutions`
--

INSERT INTO `_evolutions` (`id`, `title`, `titleDown`, `description`, `batch`, `checksum`, `status`, `created`, `created_at`, `updated_at`) VALUES
(1, '20200224_131832.up.sql', '20200224_131832.down.sql', 'C:/Users/User/api_chambita/server/tool/primary/migrations/20200224_131832.up.sql', NULL, NULL, 0, NULL, NULL, NULL);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `empleadorcalificaciones`
--
ALTER TABLE `empleadorcalificaciones`
  ADD PRIMARY KEY (`idCalificacion`),
  ADD KEY `dniEmpleador` (`dniEmpleador`),
  ADD KEY `dniTrabajador` (`dniTrabajador`);

--
-- Indices de la tabla `empleadores`
--
ALTER TABLE `empleadores`
  ADD PRIMARY KEY (`dniEmpleador`);

--
-- Indices de la tabla `etiquetas`
--
ALTER TABLE `etiquetas`
  ADD PRIMARY KEY (`idEtiqueta`);

--
-- Indices de la tabla `horarios`
--
ALTER TABLE `horarios`
  ADD PRIMARY KEY (`idHorario`),
  ADD KEY `idTrabajo` (`idTrabajo`);

--
-- Indices de la tabla `sessions`
--
ALTER TABLE `sessions`
  ADD PRIMARY KEY (`sid`),
  ADD KEY `sessions_expired_index` (`expired`);

--
-- Indices de la tabla `trabajadorcalificaciones`
--
ALTER TABLE `trabajadorcalificaciones`
  ADD PRIMARY KEY (`idCalificacion`),
  ADD KEY `idEmpleador` (`idEmpleador`),
  ADD KEY `idTrabajador` (`idTrabajador`);

--
-- Indices de la tabla `trabajadores`
--
ALTER TABLE `trabajadores`
  ADD PRIMARY KEY (`dni`);

--
-- Indices de la tabla `trabajador_etiquetas`
--
ALTER TABLE `trabajador_etiquetas`
  ADD PRIMARY KEY (`idRelacion`),
  ADD KEY `dniTrabajador` (`dniTrabajador`),
  ADD KEY `idEtiqueta` (`idEtiqueta`);

--
-- Indices de la tabla `trabajador_trabajos`
--
ALTER TABLE `trabajador_trabajos`
  ADD PRIMARY KEY (`idRelacion`),
  ADD KEY `dniTrabajador` (`dniTrabajador`),
  ADD KEY `idTrabajo` (`idTrabajo`);

--
-- Indices de la tabla `trabajos`
--
ALTER TABLE `trabajos`
  ADD PRIMARY KEY (`idTrabajo`),
  ADD KEY `dniEmpleador` (`dniEmpleador`);

--
-- Indices de la tabla `trabajo_etiquetas`
--
ALTER TABLE `trabajo_etiquetas`
  ADD PRIMARY KEY (`idRelacion`),
  ADD KEY `idEtiqueta` (`idEtiqueta`),
  ADD KEY `idTrabajo` (`idTrabajo`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `_evolutions`
--
ALTER TABLE `_evolutions`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `empleadorcalificaciones`
--
ALTER TABLE `empleadorcalificaciones`
  MODIFY `idCalificacion` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `etiquetas`
--
ALTER TABLE `etiquetas`
  MODIFY `idEtiqueta` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `horarios`
--
ALTER TABLE `horarios`
  MODIFY `idHorario` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `trabajadorcalificaciones`
--
ALTER TABLE `trabajadorcalificaciones`
  MODIFY `idCalificacion` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `trabajador_etiquetas`
--
ALTER TABLE `trabajador_etiquetas`
  MODIFY `idRelacion` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `trabajador_trabajos`
--
ALTER TABLE `trabajador_trabajos`
  MODIFY `idRelacion` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `trabajos`
--
ALTER TABLE `trabajos`
  MODIFY `idTrabajo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `trabajo_etiquetas`
--
ALTER TABLE `trabajo_etiquetas`
  MODIFY `idRelacion` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `_evolutions`
--
ALTER TABLE `_evolutions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `empleadorcalificaciones`
--
ALTER TABLE `empleadorcalificaciones`
  ADD CONSTRAINT `empleadorcalificaciones_ibfk_1` FOREIGN KEY (`dniEmpleador`) REFERENCES `empleadores` (`dniEmpleador`),
  ADD CONSTRAINT `empleadorcalificaciones_ibfk_2` FOREIGN KEY (`dniTrabajador`) REFERENCES `trabajadores` (`dni`);

--
-- Filtros para la tabla `horarios`
--
ALTER TABLE `horarios`
  ADD CONSTRAINT `horarios_ibfk_1` FOREIGN KEY (`idTrabajo`) REFERENCES `trabajos` (`idTrabajo`);

--
-- Filtros para la tabla `trabajadorcalificaciones`
--
ALTER TABLE `trabajadorcalificaciones`
  ADD CONSTRAINT `trabajadorcalificaciones_ibfk_1` FOREIGN KEY (`idEmpleador`) REFERENCES `empleadores` (`dniEmpleador`),
  ADD CONSTRAINT `trabajadorcalificaciones_ibfk_2` FOREIGN KEY (`idTrabajador`) REFERENCES `trabajadores` (`dni`);

--
-- Filtros para la tabla `trabajador_etiquetas`
--
ALTER TABLE `trabajador_etiquetas`
  ADD CONSTRAINT `trabajador_etiquetas_ibfk_1` FOREIGN KEY (`dniTrabajador`) REFERENCES `trabajadores` (`dni`),
  ADD CONSTRAINT `trabajador_etiquetas_ibfk_2` FOREIGN KEY (`idEtiqueta`) REFERENCES `etiquetas` (`idEtiqueta`);

--
-- Filtros para la tabla `trabajador_trabajos`
--
ALTER TABLE `trabajador_trabajos`
  ADD CONSTRAINT `trabajador_trabajos_ibfk_1` FOREIGN KEY (`dniTrabajador`) REFERENCES `trabajadores` (`dni`),
  ADD CONSTRAINT `trabajador_trabajos_ibfk_2` FOREIGN KEY (`idTrabajo`) REFERENCES `trabajos` (`idTrabajo`);

--
-- Filtros para la tabla `trabajos`
--
ALTER TABLE `trabajos`
  ADD CONSTRAINT `trabajos_ibfk_1` FOREIGN KEY (`dniEmpleador`) REFERENCES `empleadores` (`dniEmpleador`);

--
-- Filtros para la tabla `trabajo_etiquetas`
--
ALTER TABLE `trabajo_etiquetas`
  ADD CONSTRAINT `trabajo_etiquetas_ibfk_1` FOREIGN KEY (`idEtiqueta`) REFERENCES `etiquetas` (`idEtiqueta`),
  ADD CONSTRAINT `trabajo_etiquetas_ibfk_2` FOREIGN KEY (`idTrabajo`) REFERENCES `trabajos` (`idTrabajo`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
